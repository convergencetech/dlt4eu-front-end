const { assert } = require('chai');

const BN = require('bn.js');

const ArganOil = artifacts.require('./ArganOil.sol')

require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bn')(BN))
    .should();

contract('ArganOil', (accounts) => {
    let contract
    let fakeAccount1

    before(async() => {
        contract = await ArganOil.deployed()
        fakeAccount1 = await web3.eth.accounts.privateKeyToAccount('0x348ce564d427a3311b6536bbcff9390d69395b06ed6c486954e971d960fe8709');
        console.log("Fake account 1 address: " + fakeAccount1.address);
    })

    describe('Deployment', async () => {
        it('Deployed succesfully', async () => {
            const address = contract.address
            assert.notEqual(address, 0x0)
            assert.notEqual(address, '')
            assert.notEqual(address, null)
            assert.notEqual(address, undefined)
        })

        it ('Correct name', async () => {
            const name = await contract.name()
            assert.equal(name, 'ArganOil')
        })

        it ('Correct symbol', async () => {
            const symbol = await contract.symbol()
            assert.equal(symbol, 'ARGO')
        })
    })


    describe('Minting', async () => {

        it('Created token', async () => {
            let oldSupply = await contract.totalSupply()
            oldSupply = new BN(oldSupply.toString())
            const result = await contract.mint(124342, "1000", 100, web3.utils.fromAscii("12123123312312"))
            let newSupply = await contract.totalSupply()
            newSupply = new BN(newSupply.toString())
            assert.equal(newSupply.toString(), oldSupply.add(new BN('1')).toString(), 'Supply did not increase')
            const event = result.logs[0].args
            assert.equal(event.tokenId.toNumber(), 1, 'ID is not correct')
            assert.equal(event.from, '0x0000000000000000000000000000000000000000', 'From is not correct')
            assert.equal(event.to, accounts[0], 'To is not correct')
        })

        it('Can get volume', async () => {
            const result = await contract.mint(124342, "type", 100, web3.utils.fromAscii("12123123312312"))
            const args = result.logs[0].args;
            const volume = await contract.getVolume(args.tokenId);
            assert.isNotNull(volume);
            assert.equal(volume.toString(), 124342);
        })

        it('Can get price', async () => {
            const result = await contract.mint(124342, "type", 100, web3.utils.fromAscii("12123123312312"))
            const args = result.logs[0].args;
            const price = await contract.getPrice(args.tokenId);
            assert.isNotNull(price);
            assert.equal(price.toString(), 100);
        })

        it('Can get oil type', async () => {
            const result = await contract.mint(124342, "type", 100, web3.utils.fromAscii("12123123312312"))
            const args = result.logs[0].args;
            const type = await contract.getOilType(args.tokenId);
            assert.isNotNull(type);
            assert.equal(type.toString(), "type");
        })

        it('Can get full info object', async () => {
            const result = await contract.mint(124342, "type", 100, web3.utils.fromAscii("12123123312312"))
            const args = result.logs[0].args;
            const info = await contract.getOilInfo(args.tokenId);
            assert.isNotNull(info);
            assert.equal(info.volume, 124342);
            assert.equal(info.oilType, "type");
            assert.equal(info.price, 100);
        })

        it('Delegated Minting', async () => {
            let oldSupply = await contract.totalSupply()
            oldSupply = new BN(oldSupply.toString())
            let accountAddress = fakeAccount1.address
            let volume = 1000
			let oilType = "type"
			let price = 100
            let hashedCredentials = web3.utils.soliditySha3("testingFAkecredentialsstuff1242h9d12idnkj")
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature

            const result = await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            let newSupply = await contract.totalSupply()
            newSupply = new BN(newSupply.toString())

            assert.equal(newSupply.toString(), oldSupply.add(new BN('1')).toString(), 'Supply did not increase')
            const event = result.logs[0].args
            assert.equal(event.from, '0x0000000000000000000000000000000000000000', 'From is correct')
            assert.equal(event.to, accountAddress, 'To is correct')
        })

        it('Delegated Minting (fail) - owner did not sign', async () => {
            let accountAddress = "0x434333a50538e3816CBbBEdCB40fE15ea8Ca74f1"
            let volume = 1000
			let oilType = "type"
			let price = 100
            let hashedCredentials = web3.utils.soliditySha3("testingFAkecredentialsstuff1242h9d12idnkj")
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, "0x9f28372afc955c53d5e9e4d63f9cf6bb96f91cafcc60be1bcc86f2a7dfac02cd")
            signedMessage = signedMessage.signature

            let err = null
            try {
                await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            } catch (error) {
                err = error
            }
            assert.ok(err instanceof Error)
        })

    })

    describe('Transfer', async () => {

        it('Delegated Transfer', async () => {
            let toAddress = "0x434333a50538e3816CBbBEdCB40fE15ea8Ca74f1"
            /* Delegate mint new token */
            let accountAddress = fakeAccount1.address
            let toAccountBalance = await contract.balanceOf(toAddress)
            toAccountBalance = new BN(toAccountBalance)
            let volume = 1000
			let oilType = "type"
			let price = 100
            let hashedCredentials = web3.utils.soliditySha3("testingFAkecredentialsstuff1242h9d12idnkj")
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            const tokenMinted = await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            const tokenID = tokenMinted.logs[0].args.tokenId.toNumber();
            /***************************/
            message = await web3.utils.soliditySha3(toAddress, tokenID)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature            
            await contract.delegatedTransferFrom(accountAddress, toAddress, tokenID, signedMessage);
            /**************************/
            let toAccountBalanceNew = await contract.balanceOf(toAddress)
            let newOwner = await contract.ownerOf(tokenID)
            assert.equal(toAccountBalanceNew.toString(), toAccountBalance.add(new BN('1')).toString(), 'To account did not recieve a token')
            assert.equal(newOwner, toAddress, 'To address is not the owner')
        })

        it('Delegated Transfer (fail) - not owner', async () => {
            let toAddress = "0x434333a50538e3816CBbBEdCB40fE15ea8Ca74f1"
            /* Delegate mint new token */
            let accountAddress = accounts[5]// Wrong account holder
            let volume = 1000
			let oilType = "type"
			let price = 100
            let hashedCredentials = web3.utils.soliditySha3("testingFAkecredentialsstuff1242h9d12idnkj")
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            const tokenMinted = await contract.delegatedMint(fakeAccount1.address, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            const tokenID = tokenMinted.logs[0].args.tokenId.toNumber();
            /***************************/
            message = await web3.utils.soliditySha3(toAddress, tokenID)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            let err = null
            try {
                await contract.delegatedTransferFrom(accountAddress, toAddress, tokenID, signedMessage);
            } catch (error) {
                err = error
            }
            assert.ok(err instanceof Error)
        })

    })

    describe('Sale', async () => {

        it('Delegated sale', async () => {
            /* Delegate mint new token */
            let accountAddress = fakeAccount1.address
            let volume = 1000
			let oilType = "type"
			let price = 100
            let credentials = "testingFAkecredentialsstuff1242h9d12idnkj";
            let hashedCredentials = web3.utils.soliditySha3(credentials)
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            const tokenMinted = await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            const tokenID = tokenMinted.logs[0].args.tokenId.toNumber();
            /***************************/
            let verifyHash = await web3.utils.soliditySha3(tokenID, accountAddress, volume, oilType, price, nonce)
            message = await web3.utils.soliditySha3(tokenID, verifyHash)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            let signature = signedMessage.signature
            let result = await contract.delegatedSale(accountAddress, tokenID, verifyHash, signature);
            assert.isNotNull(result);
            result = await contract.isOilForSale(tokenID);
            assert.isTrue(result);
        })

        it('Delegated sale (fail) - not owner', async () => {
            /* Delegate mint new token */
            let accountAddress = fakeAccount1.address
            let volume = 1000
			let oilType = "type"
			let price = 100
            let credentials = "testingFAkecredentialsstuff1242h9d12idnkj";
            let hashedCredentials = web3.utils.soliditySha3(credentials)
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            const tokenMinted = await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            const tokenID = tokenMinted.logs[0].args.tokenId.toNumber();
            /***************************/
            let verifyHash = await web3.utils.soliditySha3(tokenID, accountAddress, volume, oilType, price, nonce)
            message = await web3.utils.soliditySha3(tokenID, verifyHash)
            signedMessage = await web3.eth.accounts.sign(message, "0x9f28372afc955c53d5e9e4d63f9cf6bb96f91cafcc60be1bcc86f2a7dfac02cd")
            let signature = signedMessage.signature;
            let err = null
            try {
                await contract.delegatedSale(accountAddress, tokenID, verifyHash, signature);
            } catch (error) {
                err = error
            }
            assert.ok(err instanceof Error)
        })

        it('Delegated sale (fail) - cannot transfer', async () => {
            let toAddress = "0x434333a50538e3816CBbBEdCB40fE15ea8Ca74f1"
            /* Delegate mint new token */
            let accountAddress = fakeAccount1.address
            let volume = 1000
			let oilType = "type"
			let price = 100
            let credentials = "testingFAkecredentialsstuff1242h9d12idnkj";
            let hashedCredentials = web3.utils.soliditySha3(credentials)
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            const tokenMinted = await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            const tokenID = tokenMinted.logs[0].args.tokenId.toNumber();
            /***************************/
            /* Put up for sale */
            let verifyHash = await web3.utils.soliditySha3(tokenID, "0xE9F8C28026B342Db2E7194505B1f3777935106c7", volume, oilType, price, nonce)
            message = await web3.utils.soliditySha3(tokenID, verifyHash)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            let signature = signedMessage.signature
            let result = await contract.delegatedSale(accountAddress, tokenID, verifyHash, signature);
            assert.isNotNull(result);
            result = await contract.isOilForSale(tokenID);
            assert.isTrue(result);
            /***************************/
            message = await web3.utils.soliditySha3(toAddress, tokenID)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            let err = null
            try {
                await contract.delegatedTransferFrom(accountAddress, toAddress, tokenID, signedMessage);
            } catch (error) {
                err = error
            }
            assert.ok(err instanceof Error)
        })

    })

    describe('Claim', async () => {

        it('Delegated claim', async () => {
            /* Delegate mint new token */
            let accountAddress = fakeAccount1.address
            let volume = 1000
			let oilType = "type"
			let price = 100
            let credentials = "testingFAkecredentialsstuff1242h9d12idnkj";
            let hashedCredentials = web3.utils.soliditySha3(credentials)
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            const tokenMinted = await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            const tokenID = tokenMinted.logs[0].args.tokenId.toNumber();
            /***************************/
            let receiver = "0xE9F8C28026B342Db2E7194505B1f3777935106c7";
            let receiverPrivateKey = "ba03d1ff9f00f3fba426ac275ded5ded567e3053fac9fd94ecc1f6be81bdefd3";
            /* Put up for sale */
            let verifyHash = await web3.utils.soliditySha3(tokenID, volume, oilType, price, nonce)
            message = await web3.utils.soliditySha3(tokenID, verifyHash)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            let signature = signedMessage.signature
            let result = await contract.delegatedSale(accountAddress, tokenID, verifyHash, signature);
            assert.isNotNull(result);
            result = await contract.isOilForSale(tokenID);
            assert.isTrue(result);
            /***************************/
            message = await web3.utils.soliditySha3(tokenID, nonce)
            signedMessage = await web3.eth.accounts.sign(message, receiverPrivateKey)
            signature = signedMessage.signature
            result = await contract.delegatedClaim(receiver, tokenID, nonce, signature);
            result = await contract.isOilForSale(tokenID);
            assert.isFalse(result);
            result = await contract.ownerOf(tokenID);
            assert.equal(result, receiver);
        })

        it('Delegated claim (fail) - not owner', async () => {
            /* Delegate mint new token */
            let accountAddress = fakeAccount1.address
            let volume = 1000
			let oilType = "type"
			let price = 100
            let credentials = "testingFAkecredentialsstuff1242h9d12idnkj";
            let hashedCredentials = web3.utils.soliditySha3(credentials)
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            const tokenMinted = await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            const tokenID = tokenMinted.logs[0].args.tokenId.toNumber();
            /***************************/
            let receiver = "0xE9F8C28026B342Db2E7194505B1f3777935106c7";
            let receiverPrivateKey = "ba03d1ff9f00f3fba426ac275ded5ded567e3053fac9fd94ecc1f6be81bdefd3";
            /* Put up for sale */
            let verifyHash = await web3.utils.soliditySha3(tokenID, volume, oilType, price, nonce)
            message = await web3.utils.soliditySha3(tokenID, verifyHash)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            let signature = signedMessage.signature
            let result = await contract.delegatedSale(accountAddress, tokenID, verifyHash, signature);
            assert.isNotNull(result);
            result = await contract.isOilForSale(tokenID);
            assert.isTrue(result);
            /***************************/
            message = await web3.utils.soliditySha3(tokenID, nonce)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signature = signedMessage.signature
            let err = null;
            try {
                await contract.delegatedClaim(receiver, tokenID, nonce, signature);
            } catch (error) {
                err = error
            }
            assert.ok(err instanceof Error)
        })

        it('Delegated claim (fail) - not the designated wallet', async () => {
            /* Delegate mint new token */
            let accountAddress = fakeAccount1.address
            let volume = 1000
			let oilType = "type"
			let price = 100
            let credentials = "testingFAkecredentialsstuff1242h9d12idnkj";
            let hashedCredentials = web3.utils.soliditySha3(credentials)
            const nonce = web3.utils.soliditySha3(volume, oilType, price, hashedCredentials, Math.random()*100000000000|0)
            let message = await web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
            let signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            signedMessage = signedMessage.signature
            const tokenMinted = await contract.delegatedMint(accountAddress, volume, oilType, price, hashedCredentials, nonce, signedMessage)
            const tokenID = tokenMinted.logs[0].args.tokenId.toNumber();
            /***************************/
            let receiver = "0xE9F8C28026B342Db2E7194505B1f3777935106c7";
            let receiverPrivateKey = "ba03d1ff9f00f3fba426ac275ded5ded567e3053fac9fd94ecc1f6be81bdefd3";
            /* Put up for sale */
            let verifyHash = await web3.utils.soliditySha3(tokenID, volume, oilType, price, nonce)
            message = await web3.utils.soliditySha3(tokenID, verifyHash)
            signedMessage = await web3.eth.accounts.sign(message, fakeAccount1.privateKey)
            let signature = signedMessage.signature
            let result = await contract.delegatedSale(accountAddress, tokenID, verifyHash, signature);
            assert.isNotNull(result);
            result = await contract.isOilForSale(tokenID);
            assert.isTrue(result);
            /***************************/
            message = await web3.utils.soliditySha3(tokenID, nonce)
            signedMessage = await web3.eth.accounts.sign(message, receiverPrivateKey)
            signature = signedMessage.signature
            let err = null;
            try {
                await contract.delegatedClaim(accountAddress, tokenID, nonce, signature);
            } catch (error) {
                err = error
            }
            assert.ok(err instanceof Error)
        })

    })

});