import React from 'react';
import { Button } from '@material-ui/core';
import { Link } from "react-router-dom"
import QR from '../QR'

function ArganOilForSale({id, oilStore, active}) {

    let modal;

    if (active) {
        modal = (
            <div className="modal">
                <div className="modal-content"> 
                    <h1>Oil succesfully locked for sale</h1>
                    <h3>The buyer can now claim the oil with the QR code below:</h3>
                    <QR data={oilStore.getQrData(id)} />
                    <Button size="large" variant="contained" color="primary">
                        <Link to="/" className="menu-item">Ok</Link>
                    </Button>
                </div>
            </div>
            )
    } else {
        modal = (<div></div>);
    }

    return modal;
}

export default ArganOilForSale;
