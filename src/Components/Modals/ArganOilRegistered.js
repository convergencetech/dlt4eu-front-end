import React from 'react';
import { Button } from '@material-ui/core';
import { Link } from "react-router-dom"

function ArganOilRegistered({active}) {

    let modal;

    if (active) {
        modal = (
            <div className="modal">
                <div className="modal-content"> 
                    <h1>Registration Complete</h1>
                    <img className="certified_tick" src="/assets/certified_tick.jpg" alt="certified tick"></img>
                    <h3 className="center-text">You can now sell your certified argan oil</h3>
                    <Button size="large" variant="contained" color="primary">
                        <Link to="/" className="menu-item">Ok</Link>
                    </Button>
                </div>
            </div>
            )
    } else {
        modal = (<div></div>);
    }

    return modal;
}

export default ArganOilRegistered;
