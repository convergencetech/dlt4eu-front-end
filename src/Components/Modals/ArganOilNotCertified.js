import React from 'react';
import { Button } from '@material-ui/core';
import { Link } from "react-router-dom"

function ArganOilNotCertified({active, setModalActive}) {

    let modal;

    if (active) {
        modal = (
            <div className="modal">
                <div className="modal-content"> 
                    <img className="not_certified_stamp" src="/assets/not_certified_oil_stamp.jpg" alt="not certified oil"></img>
                    <h3>Argan oil certification claims cannot be confirmed.</h3>
                    <Button size="large" variant="contained" color="primary" onClick={() => {setModalActive(false)}}>Try Again</Button>
                </div>
            </div>
            )
    } else {
        modal = (<div></div>);
    }

    return modal;
}

export default ArganOilNotCertified;
