import React from 'react';
import {Switch, Route} from "react-router-dom"
import DashboardView from '../../Views/Dashboard';
import HomeView from '../../Views/Home';
import MarketplaceView from '../../Views/Marketplace';
import RegisterOilView from '../../Views/RegisterOil';
import SellOilView from '../../Views/SellOil';
import ViewQRView from '../../Views/ViewQR';
import ScanOilView from '../../Views/ScanOil';
import OilView from '../../Views/Oil';
import OilInfo from '../../Views/OilInfo';


function NavigationSwitch({credentialStore, oilStore, user}) {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <HomeView user={user} oilStore={oilStore} credentialStore={credentialStore}/>
        </Route>
        <Route path="/dashboard">
          <DashboardView credentialStore={credentialStore} oilStore={oilStore}/>
        </Route>
        <Route path="/marketplace">
          <MarketplaceView />
        </Route>

        
        <Route path="/register-oil">
          <RegisterOilView  credentialStore={credentialStore} oilStore={oilStore}/>
        </Route>
        <Route path="/sell-oil/:id">
          <SellOilView oilStore={oilStore}/>
        </Route>
        <Route path="/view-qr/:id/:type">
          <ViewQRView oilStore={oilStore} credentialStore={credentialStore}/>
        </Route>
        <Route path="/claim-oil">
          <ScanOilView oilStore={oilStore} credentialStore={credentialStore}/>
        </Route>
        <Route path="/oil/:id">
          <OilView credentialStore={credentialStore} oilStore={oilStore}/>
        </Route>
        <Route path="/oilinfo">
          <OilInfo credentialStore={credentialStore} oilStore={oilStore}/>
        </Route>
      </Switch>
    </div>
  );
}

export default NavigationSwitch;
