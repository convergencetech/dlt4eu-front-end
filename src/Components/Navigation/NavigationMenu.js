import React from 'react';
import {Link} from "react-router-dom"

function NavigationMenu({setAuthenticated}) {
  return (
    <div>
        <ul className="menu-container">
          <li>
            <Link to="/" className="menu-item">Home</Link>
          </li>
          <li>
            <Link to="/dashboard" className="menu-item">Dashboard</Link>
          </li>
          {/* <li>
            <Link to="/marketplace" className="menu-item">Marketplace</Link>
          </li> */}
          <li>
            <Link to="/" onClick={() => setAuthenticated(false)} className="menu-item">Logout</Link>
          </li>
        </ul>
    </div>
  );
}

export default NavigationMenu;
