import React from 'react';
import axios from 'axios';
import dummyCredential1 from './DummyCredential_1.json';
import dummyCredential2 from './DummyCredential_2.json';

class CredentialStore extends React.Component {
  
  constructor(){
    super()
    this.credentials = []
    this.oilCredentials = {}
    this.createBaseCredential();
    this.loadCredentials();
    if (this.credentials === null || this.credentials.length === 0) {
      this.addCredential(JSON.stringify(dummyCredential1)); // Add dummy credential if none exist
      this.addCredential(JSON.stringify(dummyCredential2)); // Add dummy credential if none exist
    }
    this.loadDID();
  }

  createBaseCredential() {
    this.baseOilCredential = {
      "credential": {
        "@context": [
          "https://www.w3.org/2018/credentials/v1",
          {
            "credentials": "https://schema.org/ItemList"
          }
        ],
        "id": "urn:uuid:b283ca6c-134f-4d3e-a46a-0a5c312b5e2b",
        "type": [
          "VerifiableCredential"
        ],
        "issuer": {
          //"id": "did:key:z6MkgHxqyP3Y8ag2fRpJHZwxwecGFm21usgUyvjfZdfEabdW"
        },
        "issuanceDate": "2020-12-02T17:25:56.985Z",
        "credentialSubject": {
          "id": "did:key:z6MkisNGAAR8Pc9HP1CK1WbzcdCV9Pjp36kEGFijotipw4cV",
          "type": "Oil NFT"
        },
        //"@type": "ItemList",
        "credentials": [ //ids
          // 1,
          // 2,
          // 3
        ],
      }
    }
  }

  loadDID() {
    let didKey = localStorage.getItem("didKey");
    if (didKey === null) {
      axios.get(`${process.env.REACT_APP_TRYBE_END_POINT}/credentials/key`).then(response => {
        console.log(response);
        this.didKey = response.data.did;
        localStorage.setItem("didKey", this.didKey)
        this.baseOilCredential["credential"]["issuer"]["id"] = this.didKey;
      })
    } else {
      this.didKey = didKey;
      this.baseOilCredential["credential"]["issuer"]["id"] = this.didKey;
    }
  }

  getCredentialById(id) {
    for (let credential of this.credentials) {
      if (credential.savedCredentials[0].id === id) return credential.issuedCredentials[0].vc;
    }
    return axios.get(`${process.env.REACT_APP_TRYBE_END_POINT}/public/credential/`+id).then(response => {
      return JSON.parse(response.data).vc;
    })
  }

  createOilCredential(id, activeCredentials) {
    this.baseOilCredential["credential"]["credentialSubject"]["id"] = id;
    //this.baseOilCredential["credential"]["id"] = id;
    this.baseOilCredential["credential"]["issuanceDate"] = new Date().toISOString();
    this.baseOilCredential["credential"]["credentials"] = [];
    for (let id of activeCredentials) {
      this.baseOilCredential["credential"]["credentials"].push(this.credentials[id]["savedCredentials"][0]["id"]);
    }
    console.log(this.baseOilCredential);
    axios.post(`${process.env.REACT_APP_TRYBE_END_POINT}/credentials/issue`, this.baseOilCredential).then(response => {
      let credential = response.data;
      this.oilCredentials[credential["credentialSubject"]["id"]] = credential;
      localStorage.setItem("oilCredentials", JSON.stringify(this.oilCredentials));
      
      let qrDataList = JSON.parse(localStorage.getItem("qrDataList"));
      qrDataList[id]["credential"] = credential;
      localStorage.setItem("qrDataList", JSON.stringify(qrDataList));
    })
  }

  loadCredentials() {
    let credentials = localStorage.getItem("credentials");
    if (credentials != null) {
      let parsedData = JSON.parse(credentials)
      this.credentials = parsedData
    }

    let oilCredentials = localStorage.getItem("oilCredentials");
    if (oilCredentials != null) {
      let parsedData = JSON.parse(oilCredentials)
      this.oilCredentials = parsedData
    }
  }

  storeCredentials() {
    console.log(this.credentials)
    localStorage.setItem("credentials", JSON.stringify(this.credentials))
  }

  addCredential(data) {
    this.credentials.push(JSON.parse(data))
    this.storeCredentials()
  }

  deleteCredential(id) {
    this.credentials.splice(id, 1)
    this.storeCredentials()
  }

  loadOilCredentialFromClaim(data) {
    console.log(data);
    let oilId = data.credentialSubject.id;
    console.log(this.oilCredentials);
    if (this.oilCredentials[oilId] === undefined) { //New oil
      console.log("ADDDINGNGNGNG");
      this.oilCredentials[oilId] = data;
    }
    console.log(this.oilCredentials);
    localStorage.setItem("oilCredentials", JSON.stringify(this.oilCredentials));
  }

  //JWT DECODER
  decodeJWT(id) {
    const token = this.credentials[id] //format changed
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return JSON.parse(jsonPayload);
  }

}

export default CredentialStore;
