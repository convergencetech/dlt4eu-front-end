import React from 'react';
import NavigationMenu from '../Navigation/NavigationMenu'

function Header({setAuthenticated}) {
  return (
    <div className="header">
      <NavigationMenu setAuthenticated={setAuthenticated} />
    </div>
  );
}

export default Header;
