import React from 'react';
import axios from 'axios';

class OilStore extends React.Component {
  
  constructor(user, web3, arganOilContract){
    super()
    this.user = user
    this.web3 = web3
    this.oil = []
    this.arganOilContract = arganOilContract
    this.loadOil()
  }

  async loadOil() {
    let oilJSON = localStorage.getItem("oilList");
    if (oilJSON !== null && oilJSON !== undefined) this.oil = JSON.parse(oilJSON);

    const supply = await this.arganOilContract.methods
    .totalSupply().call({ from: this.user.address });
    console.log(`Oil minted: ${supply}`);

    let userBalance =  await this.arganOilContract.methods
    .balanceOf(this.user.address).call({ from: this.user.address });
    console.log(`User balance ${userBalance}`);

    this.oil = []
    for (let i = 1; i <= supply; i++) {
      let info =  await this.arganOilContract.methods
      .getOilInfo(i).call({ from: this.user.address });
      if (info.currentOwner === this.user.address || info.minter === this.user.address) { //either own the oil or have minted it
        this.oil.push({
          id: i,
          credentialsProof: info.credentialsProof,
          volume: info.volume,
          oilType: info.oilType,
          price: info.price,
          saleVerify: info.saleVerify,
          stillOwn: (info.currentOwner === this.user.address) ? true : false
        });
      }
      localStorage.setItem("oilList", JSON.stringify(this.oil));
    }

    console.log("LOADED NEW OIL DATA");
    console.log(this.oil);
    return true;
  }

  async mintOil(volume, oilType, price, credentialsPicked, credentials) {
    let hashedCredentials = await this.web3.utils.soliditySha3(credentialsPicked.join('')) //TODO
    const nonce = await this.web3.utils.soliditySha3(volume, oilType, price, hashedCredentials,  Math.random()*1000000000000000|0)
    let message = await this.web3.utils.soliditySha3(volume, oilType, price, nonce, hashedCredentials)
    let signedMessage = await this.web3.eth.accounts.sign(message, this.user.privateKey)
    let signature = signedMessage.signature
    let packet = {
      accountAddress: this.user.address, 
      volume: volume,
      oilType: oilType,
      price: price,
      credentials: hashedCredentials,
      nonce: nonce,
      signedMessage: signature
    }
    console.log(packet)
    let result = await axios.post(`${process.env.REACT_APP_SERVER_URL}/${process.env.REACT_APP_API_VERSION}/mintOil`, { packet })
      .then(res => {
        console.log("Oil minted");
        let result = res.data.result;
        console.log(result);
        this.storeQRdata(credentials, credentialsPicked, volume, oilType, price, result.events.Transfer.returnValues.tokenId)
        return result;
      })
    await this.loadOil()
    return result;
  }

  async sellOil(id, address) {
    let qrDataList = JSON.parse(localStorage.getItem("qrDataList"));
    let qrData = qrDataList[id];
    //let verifyHash = await this.web3.utils.soliditySha3(id, address, qrData.weight, qrData.volume, qrData.nonce)
    let verifyHash = await this.web3.utils.soliditySha3(id, qrData.volume, qrData.oilType, qrData.price, qrData.nonce)
    let message = await this.web3.utils.soliditySha3(id, verifyHash)
    let signedMessage = await this.web3.eth.accounts.sign(message, this.user.privateKey)
    let signature = signedMessage.signature
    let packet = {
      accountAddress: this.user.address,
      id,
      verifyHash,
      signature
    }
    await axios.post(`${process.env.REACT_APP_SERVER_URL}/${process.env.REACT_APP_API_VERSION}/sellOil`, { packet })
      .then(res => {
        console.log("Oil up for sale");
        let result = res.data.result;
        console.log(result);
      })
    await this.loadOil()
    return true
  }

  async claimOil(data) {
    data = JSON.parse(data);
    let message = await this.web3.utils.soliditySha3(data.id, data.nonce)
    let signedMessage = await this.web3.eth.accounts.sign(message, this.user.privateKey)
    let signature = signedMessage.signature
    let packet = {
      accountAddress: this.user.address,
      id: data.id,
      nonce: data.nonce,
      signature
    }
    await axios.post(`${process.env.REACT_APP_SERVER_URL}/${process.env.REACT_APP_API_VERSION}/claimOil`, { packet })
      .then(res => {
        console.log("Claimed oil");
        let result = res.data.result;
        console.log(result);
      })
    await this.loadOil()
    return true 
  }






  async getOilById(id) {
    for (let oil of this.oil) {
      if (oil.id === parseInt(id)) return oil;
    }
    return await this.arganOilContract.methods
    .getOilInfo(id).call({ from: this.user.address }).then((result) => {
      let oil = {
        id: id,
        credentialsProof: result.credentialsProof,
        volume: result.volume,
        oilType: result.oilType,
        price: result.price,
        saleVerify: result.saleVerify,
      };
      return oil;
    });
  }





  /* QR util */

  async storeQRdata(credentials, credentialsArray, volume, oilType, price, id) {
    let qrDataList = JSON.parse(localStorage.getItem("qrDataList"));
    if (qrDataList === null) {
      qrDataList = {}
    }
    const qrCredentials = []
    for (let i = 0; i < credentialsArray.length; i++) {
      const index = credentialsArray[i]
      qrCredentials.push(credentials[index])
    }
    let qrData = {
      credentials: qrCredentials,
      volume,
      oilType,
      price,
      nonce: await this.web3.utils.soliditySha3(Math.random()*1000000000000000|0)
    }
    qrDataList[id] = qrData
    localStorage.setItem("qrDataList", JSON.stringify(qrDataList));
  }

  getQrData(id) {
    let qrDataList = JSON.parse(localStorage.getItem("qrDataList"));
    let data = qrDataList[id];
    data.credentials = [] ; //TEMP removal
    data.id = id;
    return JSON.stringify(data);
  }

}

export default OilStore;
