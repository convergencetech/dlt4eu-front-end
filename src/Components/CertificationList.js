import React from 'react';
import Certification from './Certification';

function Certificationlist({credentialStore}) {

  let credentialList = credentialStore.credentials.map((value, id) =>
    <li className="credential-item" key={value.savedCredentials[0].id}>
        <Certification certification={value.issuedCredentials[0].vc} certificateId={value.savedCredentials[0].id}/>
    </li>
  );
    
  return (
    <ul className="credential-list">
        {credentialList}
    </ul>
  );

}

export default Certificationlist;
