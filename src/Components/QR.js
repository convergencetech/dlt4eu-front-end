import React from 'react';

var QRCode = require('qrcode.react');

function QR({data}) {

  return (
    <QRCode className="qr-code" level="L" includeMargin={true} size={2048} value={data} />
  );
  
}

export default QR;
