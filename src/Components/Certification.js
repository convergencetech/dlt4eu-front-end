import React, { useState, useEffect } from 'react';

function Certification({certification, certificateId}) {

  const [certificationLoaded, setCertificationLoaded] = useState(false)
  const [vc, setVC] = useState(null)
  const [certificateURL, setCertificateURL] = useState(null)

  useEffect(() => {
    if (certification.proof !== undefined && !certificationLoaded) {
      setCertificationLoaded(true);
      setVC(certification);
    } else if (!certificationLoaded) {
      certification.then((result) => {
        setCertificationLoaded(true);
        setVC(result);
      })
    }

    if (certificateId !== null && certificateURL === null) {
      setCertificateURL(`https://staging.trybe.id/verifier?id=${certificateId}`);
    }
    
  }, [certificationLoaded, certification])

  let content = (<div></div>);

  if (certificationLoaded && vc !== null) {
    content =  (
      <a href={certificateURL} className="credential-item-info-link" target="_blank" rel="noopener noreferrer">
        <img className="credential-item-info image" src={vc.additionalData.imageUrl} alt="certification_logo"></img>
        <div className="credential-item-info-container">
          <div className="credential-item-info title">{vc.name}</div>
          <div className="credential-item-info issued">Issued: {new Date(vc.issuanceDate).toUTCString()}</div>
        </div>
      </a>
    );
  }

  return content;

}

export default Certification;
