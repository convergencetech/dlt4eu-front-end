import React, { useEffect } from 'react';
import { Button } from '@material-ui/core';
import Web3 from 'web3';

function Login({user, registerUser, setAuthenticated, autoLogIn}) {

  const web3 = new Web3(Web3.givenProvider);

  useEffect(() => {
    if(autoLogIn) {
      if (user == null) {
        createAccount();
      }
      login();
    }
  });

  let createAccount = async () => {
    let account = web3.eth.accounts.create();
    console.log(account);
    registerUser(account)
    localStorage.setItem("user", JSON.stringify(account));
  }

  let login  = async () => {
    if (user != null) {
      setAuthenticated(true)
    }
  }

  let deleteKeystore = () => {
    registerUser(null)
    localStorage.removeItem("user");
    localStorage.removeItem("qrDataList");
    localStorage.removeItem("credentials");
  }

  return (
    <div className="login-form-container">
      <Button disabled={user != null} onClick={() => createAccount()} variant="contained" color="primary">
        <h3>Create</h3>
      </Button>
      <Button disabled={user != null || true} variant="contained" color="primary">
        <h3>Restore</h3>
      </Button>
      <Button disabled={user == null} onClick={() => login()} variant="contained" color="primary">
        <h3>Login</h3>
      </Button>
      <Button disabled={user == null} onClick={() => deleteKeystore()} variant="contained" color="primary">
        <h3>Delete</h3>
      </Button>
    </div>
  );
}

export default Login;
