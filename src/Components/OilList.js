import React from 'react';
import { Button, TableContainer, Table, TableHead, TableRow, TableCell, TableBody  } from '@material-ui/core';
import { Link } from "react-router-dom"
import Paper from '@material-ui/core/Paper';
import { useHistory } from "react-router-dom";

function OilList({oilStore, stillOwn}) {

  const history = useHistory();

  let zero = "0x0000000000000000000000000000000000000000000000000000000000000000";

  // let oilList = oilStore.oil.filter((oil) => {
  //   return oil.stillOwn === stillOwn;
  // }).map((oil, id) =>
  //   <li className={`oil-item ${(oil.saleVerify !== zero)?`for-sale`:``}`} key={oil.id}>
  //       <Link to={"/oil/"+oil.id} className="oil-item-info-link">
  //       <div className="oil-item-info">ID: {oil.id}</div>
  //       <div className="oil-item-info">Volume: {oil.volume} L</div>
  //       <div className="oil-item-info">Weight: {oil.weight} KG</div>
  //       </Link>
  //   </li>
  // ); // DEPRECATED

  const handleClick = (id) => {
    history.push(`/oil/${id}`)
  }
    
  return (
    <ul className="oil-list">
        {/* {oilList} */}
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Oil ID</TableCell>
                <TableCell align="center">Oil Type</TableCell>
                <TableCell align="center">Volume</TableCell>
                <TableCell align="center">Price</TableCell>
                {/* <TableCell align="right">For Sale</TableCell> */}
              </TableRow>
            </TableHead>
            <TableBody>
              {oilStore.oil.filter((oil) => {
                return oil.stillOwn === stillOwn;
              }).map((oil) => (
                <TableRow key={oil.id} onClick={() => {handleClick(oil.id)}} className={`${(oil.saleVerify !== zero)?`for-sale`:``}`}>
                  <TableCell component="th" scope="row">
                    {oil.id}
                  </TableCell>
                  <TableCell align="center">{oil.oilType}</TableCell>
                  <TableCell align="center">{oil.volume} L</TableCell>
                  <TableCell align="center">{oil.price}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
    </ul>
  );

}

export default OilList;
