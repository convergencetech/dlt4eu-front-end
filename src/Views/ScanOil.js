import React, { useState } from 'react';
import QrReader from 'react-qr-reader'
import { TextareaAutosize } from '@material-ui/core'
import  { Redirect } from 'react-router-dom'
import { Link } from "react-router-dom"
import { useHistory } from "react-router-dom";
import ArganOilNotCertified from '../Components/Modals/ArganOilNotCertified'

function ScanOilView({oilStore, credentialStore}) {

  const [result, setResult] = useState(null)
  const [oilClaimed, setOilClaimed] = useState(false)
  const [loadingClaim, setLoadingClaim] = useState(false)
  const [activeModal, setModalActive] = useState(false)

  const history = useHistory();

  const handleScan = async (data) => {
    if (data) {
      let dataObj = JSON.parse(data);
      if (dataObj["nonce"] !== undefined) { //sale
        setLoadingClaim(true)
        setResult(await oilStore.claimOil(data));
        credentialStore.loadOilCredentialFromClaim(dataObj.credential);
        setLoadingClaim(false)
        setOilClaimed(result);
        history.push({pathname: "/"})
      } else if (dataObj["@context"] !== undefined) { //info
        history.push({
          pathname: "/oilinfo",
          state: { credential: dataObj }
        })
      } else {
        setModalActive(true)
      }
    }
  };

  const handleError = (err) => {
    console.error(err)
    //alert(err)
  };
  let redirect = (<div></div>)
  if (oilClaimed) redirect = (<Redirect to='/'/>)

  let scannerPart = (
    <QrReader
      delay={300}
      onError={handleError}
      onScan={handleScan.bind(this)}
      style={{ width: '100%' }}
    />
  );
  if (result !== null) {
    scannerPart = (
      <div className="qr-reader-placeholder"></div>
    );
  }

  const scanner = (
    <div className="qr-reader-container">
        {scannerPart}
        <p>{(result === null)?"Nothing Detected":"Detected Data"}</p>
    </div>
  )

  let devExtra = (
    <div style={{width: "100%"}}>
      <TextareaAutosize aria-label="minimum height" style={{width: "90%"}}
      rowsMin={5} onBlur={event => {handleScan(event.target.value)}} />
    </div>
  );
  if (process.env.REACT_APP_ENV !== "dev") devExtra = (<div></div>)

  if (loadingClaim) {
    return (
      <div className="claiming-oil">
        <h1>Claiming oil...</h1>
      </div>
    )
  } else {
    return (
      <div className="page">
        <ArganOilNotCertified active={activeModal} setModalActive={setModalActive} ></ArganOilNotCertified>
        <div className="close">
          <Link to={"/"}>
            <img src="/assets/close.jpg" alt="close"></img>
          </Link>
        </div>
        <h1>Scan Argan Oil:</h1>
        Please scan the QR code
        <div className="horizontal-container">
          {scanner}
        </div>
        {devExtra}
        {redirect}
      </div>
    );
  }
}

export default ScanOilView;
