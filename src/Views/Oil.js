import React, { useState } from 'react';
import { Button, TextField  } from '@material-ui/core';
import { useParams } from 'react-router-dom'
import ArganOilForSale from '../Components/Modals/ArganOilForSale';
import Certification from '../Components/Certification'
import { Link } from "react-router-dom"
import { useHistory } from "react-router-dom";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

function OilView({credentialStore, oilStore}) {

  const requireAnyData = false;
  const [disabledButton, disableButton] = useState(requireAnyData)
  const [activeModal, setModalActive] = useState(false)
  const [oil, setOil] = useState(false)
  var { id } = useParams();

  const history = useHistory();

  let zero = "0x0000000000000000000000000000000000000000000000000000000000000000";

  const viewSaleQR = () => {
    history.push("/view-qr/"+id+"/sale")
  }

  const viewInfoQR = () => {
    history.push("/view-qr/"+id+"/info")
  }

  const getOil = async () => {
    setOil(await oilStore.getOilById(id));
  }
  
  getOil();
  let oilCredential = credentialStore.oilCredentials[id];

  let sellOil = async () => {
    disableButton(true);
    const result = await oilStore.sellOil(id)
    console.log(result);
    disableButton(false);
    if (result) {
      setModalActive(true);
    }
  }

  const oilSaleForm = (
    <div className="div-table-container">
      <div className="div-table">
        <div className="div-table-column">
          <br/>
        <div className="button-group">
          <Button onClick={() => sellOil()} size="large" variant="contained" color="primary" disabled={disabledButton || (oil.stillOwn === false)}>
            {(!disabledButton) ? "Sell Oil Passport" : "Loading..."}
          </Button>
          <Button onClick={viewInfoQR} size="large" variant="contained" color="primary" disabled={disabledButton}>
            {(!disabledButton) ? "View Oil Passport" : "Loading..."}
          </Button>
        </div>
        </div>
      </div>
      <div className="div-table scrollable">
      </div>
    </div>
  )

  const showQR = (
    <div className="button-group">
      <Button onClick={viewSaleQR} size="large" variant="contained" color="primary" disabled={oil.stillOwn === false}>
        Show Sale QR
      </Button>
      <Button onClick={viewInfoQR} size="large" variant="contained" color="primary">
        View Oil Passport
      </Button>
    </div>
  );

  const certificationList = oilCredential.credentials.map((value, id) =>
    <li className="credential-item" key={value}>
        <Certification certification={credentialStore.getCredentialById(value)} certificateId={value} />
    </li>
  );

  const MapComponent = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    className="map"
    defaultZoom={9}
    defaultCenter={{ lat: 30.61411, lng: -9.34966 }}
  >
    {props.isMarkerShown && <Marker position={{ lat: 30.61411 , lng: -9.34966 }} />}
  </GoogleMap>
))

  if (oilCredential === null || oil === null) {
    return (<div></div>)
  } else {
    return (
      <div className="oil-view page">
        <div className="close">
          <Link to={"/"}>
            <img src="/assets/close.jpg" alt="close"></img>
          </Link>
        </div>
        <h1>argan oil</h1>
        <h3>{oilCredential.id.split(":")[2]}</h3>
        <div className="oil-info-container">
          <b>Date registered:</b> {new Date(oilCredential.issuanceDate).toDateString()}
        </div>
        <div className="oil-info-container">
          <b>Type:</b> {oil.oilType}
        </div>
        <div className="oil-info-container">
          <b>Volume:</b> {oil.volume} L
        </div>
        <div className="oil-info-container">
          <b>Price:</b> {oil.price}
        </div>
        <ul className="credential-list">
            {certificationList}
        </ul>
        <img className="certified_stamp" src="/assets/certified_oil_stamp.jpg" alt="certified oil"></img>
        <div className="horizontal-container">
          <ArganOilForSale id={id} oilStore={oilStore} active={activeModal} ></ArganOilForSale>
          {(oil.saleVerify === zero) ? oilSaleForm : showQR}
        </div>
        <h3 class="title">Coop information</h3>
        <div class="coof-info-container">
          <div class="coop-info logo">
            <img className="certified_stamp" src="/assets/coop_logo.jpeg" alt="coop logo"></img>
          </div>
          <div class="coop-info">
            <h3 class="subtitle">Coopérative Afoulki</h3>
            <a href="http://www.cooperativeafoulki.com">http://www.cooperativeafoulki.com</a>
          </div>
          <div class="coop-info background">
            <img className="certified_stamp" src="/assets/coop_background.png" alt="coop logo"></img>
          </div>
          <div class="coop-info">
            <ul>
              <li><b>Founded:</b> 2004</li>
              <li><b>#of cooperatives:</b> 5</li>
              <li><b>#of members:</b> 200</li>
              <li><b>Output:</b> 100T / A </li>
            </ul>
          </div>
          <MapComponent
            isMarkerShown
            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyAkbVlOwTU_rDzPXNczm6ZP51kWJxXllFA"
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `300px`, width: `100%` }} />}
            mapElement={<div style={{ height: `100%` }} />}
          />
          <div class="coop-info">
          The Afoulki cooperative has been involved since its creation in organic production in order to protect the health of consumers.
          The cooperative operates from the raw material “Afiach” to the shipment of the finished product. All stages of production are mastered in order to guarantee a high quality product and compliance with health and safety standards for both staff and the end consumer.
          Investing in infrastructure, the manufacturing process, training, supervision and the development of fundamental skills in order to improve the strategic capacity of our organization are key focuses. The success of our female cooperative project which works in rural areas allows us to fully play our role as the locomotive of human development through the creation of jobs and wealth in our areas of intervention and particularly in rural areas.
          </div>
        </div>
      </div>
    );
  } 
}

export default OilView;
