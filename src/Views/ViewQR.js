import React from 'react';
import { useParams } from 'react-router-dom'
import { TextareaAutosize } from '@material-ui/core'
import QR from '../Components/QR'
import { Button } from '@material-ui/core';
import { Link } from "react-router-dom"


var QRCode = require('qrcode.react');

function ViewQRView({oilStore, credentialStore}) {

  var { id, type } = useParams();

  let subtitle = "Have the buyer scan this QR code to see information about the oil:";
  if (type === "sale") {
    subtitle = "Have the buyer scan this QR code to buy the information about the oil:";
  }

  let data
  if (type === "sale") {
    data = oilStore.getQrData(id);
  } else if (type === "info") {
    data = JSON.stringify(credentialStore.oilCredentials[id]);
  }

  let print = () => {
    window.print();
  }

  const qr = (
    <div className="div-table-container">
      <QR data={data} />
    </div>
  )

  let devExtra = (
    <div style={{width: "100%"}}>
      <TextareaAutosize aria-label="minimum height"  style={{width: "90%"}}
      rowsMin={5} defaultValue={data} />
    </div>
  );
  if (process.env.REACT_APP_ENV !== "dev") devExtra = (<div></div>)

  return (
    <div className="dashboard-view page">
    <div className="close">
      <Link to={"/oil/"+id}>
        <img src="/assets/close.jpg" alt="close"></img>
      </Link>
    </div>
      <h1>Have the buyer scan this QR code</h1>
      <h3>{subtitle}</h3>
      <div className="horizontal-container">
        {qr}
      </div>
      <Button onClick={() => print()} variant="contained" color="primary">Print</Button>
      {devExtra}
    </div>
  );
}

export default ViewQRView;
