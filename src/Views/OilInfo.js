import React, { useState, useEffect } from 'react';
import { Button, TextField  } from '@material-ui/core';
import Certification from '../Components/Certification'
import { Link } from "react-router-dom"
import { useHistory, useLocation } from "react-router-dom";

function OilInfoView({credentialStore, oilStore}) {

  const [oil, setOil] = useState(null)
  const [oilCredential, setOilCredential] = useState(null)

  const history = useHistory();
  const location = useLocation();

  const getOil = async () => {
    setOil(await oilStore.getOilById(oilCredential.credentialSubject.id))
  }

  useEffect(() => {
    if (oilCredential === null) setOilCredential(location.state.credential);
    if (oilCredential !== null && oil === null) getOil();
  }, [oil, oilCredential])

  if (oil === null || oilCredential === null) {
    return (<div></div>)
  } else {
    console.log(oil);
    console.log(oilCredential);
    const certificationList = oilCredential.credentials.map((value, id) =>
      <li className="credential-item" key={value}>
          <Certification certification={credentialStore.getCredentialById(value)} />
      </li>
    );
    return (
      <div className="oil-view page">
        <div className="close">
          <Link to={"/"}>
            <img src="/assets/close.jpg" alt="close"></img>
          </Link>
        </div>
        <h1>argan oil</h1>
        <h3>{oilCredential.id.split(":")[2]}</h3>
        <div className="oil-info-container">
          <b>Date registered:</b> {new Date(oilCredential.issuanceDate).toDateString()}
        </div>
        <div className="oil-info-container">
          <b>Volume:</b> {oil.volume} L
        </div>
        <div className="oil-info-container">
          <b>Weight:</b> {oil.weight} KG
        </div>
        <ul className="credential-list">
            {certificationList}
        </ul>
        <img className="certified_stamp" src="/assets/certified_oil_stamp.jpg" alt="certified oil"></img>
      </div>
    );
  }
}

export default OilInfoView;
