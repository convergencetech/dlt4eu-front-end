import React, { useState } from 'react';
import { Button, TextareaAutosize, TextField, FormControl, FormLabel, FormControlLabel, RadioGroup, Radio  } from '@material-ui/core';
import Switch from '@material-ui/core/Switch';
import Chip from '@material-ui/core/Chip';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Input from '@material-ui/core/Input';
import ArganOilRegistered from '../Components/Modals/ArganOilRegistered'
import { Link } from "react-router-dom"

function RegisterOilView({credentialStore, oilStore}) {

  const [disabledButton, disableButton] = useState(true)
  const [loading, setLoading] = useState(false)
  const [activeModal, setModalActive] = useState(false)
  const [oilVolume, setOilVolume] = useState("")
  const [oilType, setOilType] = useState("cosmetic")
  const [price, setPrice] = useState("")
  const [credentialSelectOpen, setCredentialSelectOpen] = useState(false)
  const [activeCredentials, setActiveCredentials] = useState([])

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250
      }
    },
    getContentAnchorEl: null
  };

  let checkButtonDisable = (_activeCredentials) => {
    if (_activeCredentials === undefined) _activeCredentials = activeCredentials;
    if (oilVolume !== '' && oilType !== '' && price !== '' && _activeCredentials.length !== 0) {
      disableButton(false);
    } else {
      disableButton(true);
    }
  }

  const handleChange = async (event) => {
    await setActiveCredentials(event.target.value);
    checkButtonDisable(event.target.value);
    setCredentialSelectOpen(false);
  };

  const handleCredentialOpen = () => {
    setCredentialSelectOpen(true)
  }

  const handleCredentialClose = () => {
    setCredentialSelectOpen(false)
  }

  const credentialselect = (
    <Select
      multiple
      open={credentialSelectOpen}
      onOpen={handleCredentialOpen}
      onClose={handleCredentialClose}
      className="fullwidth"
      value={activeCredentials}
      onChange={handleChange}
      input={<Input id="select-multiple-chip" />}
      renderValue={(selected) => (
        <div>
          {selected.map((value) => (
            <Chip key={value} label={credentialStore.credentials[value].issuedCredentials[0].vc.name} />
          ))}
        </div>
      )}
      MenuProps={MenuProps}
    >
      {credentialStore.credentials.map((value, id) => (
        <MenuItem key={id} value={id}>
          {value.issuedCredentials[0].vc.name}
        </MenuItem>
      ))}
    </Select>
  );

  let mintOil = async () => {
    setLoading(true);
    disableButton(true);
    const result = await oilStore.mintOil(oilVolume, oilType, price, activeCredentials, credentialStore.credentials)
    console.log(result);
    credentialStore.createOilCredential(result.events.Transfer.returnValues.tokenId, activeCredentials);
    setLoading(false);
    disableButton(false);
    if (result) {
      setModalActive(true);
    }
  }

  const oilRegisterForm = (
    <div className="div-table-container">
      <div className="div-table">
        <div className="div-table-column">
          <TextField label="Volume (ml)" id="volume" type="number" value={oilVolume} onChange={event => {setOilVolume(event.target.value); checkButtonDisable();}}/>
          <TextField label="Price" id="volume" type="number" value={price} onChange={event => {setPrice(event.target.value); checkButtonDisable();}}/>
          <FormControl component="fieldset">
          <FormLabel component="legend">Oil Type</FormLabel>
          <RadioGroup aria-label="gender" name="gender1" value={oilType} onChange={event => {setOilType(event.target.value); checkButtonDisable();}}>
            <FormControlLabel value="cosmetic" control={<Radio />} label="Cosmetic" />
            <FormControlLabel value="culinary" control={<Radio />} label="Culinary" />
          </RadioGroup>
        </FormControl>
          
          <InputLabel id="demo-mutiple-chip-label">Add Product Claims</InputLabel>
          {credentialselect}
          <br/>
          <Button onClick={() => mintOil()} size="large" variant="contained" color="primary" disabled={disabledButton}>
            {(!loading) ? "Register Oil" : "Loading..."}
          </Button>
        </div>
      </div>
      {/* <div className="div-table scrollable">
      </div> */}
    </div>
  )

  return (
    <div className="dashboard-view page">
      <div className="close">
        <Link to={"/"}>
          <img src="/assets/close.jpg" alt="close"></img>
        </Link>
      </div>
      <h1>Register new Argan Oil</h1>
      <div className="horizontal-container">
        <ArganOilRegistered active={activeModal} ></ArganOilRegistered>
        {oilRegisterForm}
      </div>
    </div>
  );
}

export default RegisterOilView;
