import React, { useState, useEffect } from 'react';
import { Button } from '@material-ui/core';
import {Link} from "react-router-dom"
import OilList from '../Components/OilList'
import CertificationList from '../Components/CertificationList'

function HomeView({user, oilStore, credentialStore}) {

  const [trigger, setTrigger] = useState(0)

  useEffect(() => {
    const interval = setInterval(() => {
      setTrigger(trigger => trigger+1);//reload page to get new updates
    }, 1000);
    return () => clearInterval(interval);
  }, [])

  let oilList = (stillOwn) => {
    if (trigger > 0) {
      return (
        <OilList oilStore={oilStore} stillOwn={stillOwn}/>
      )
    } else {
      return (<div></div>)
    }
  } 

  const calculateProfit = () => {
    let totalProfit = 472.67;
    oilStore.oil.filter((oil) => {
      return oil.stillOwn === false;
    }).forEach((oil) => {
      totalProfit += parseInt(oil.price);
    })

    return `$${totalProfit}`
  }
  

  return (
    <div className="home-view page">
      <img className="header_image" src="assets/home_background.jpg" alt="background"></img>
      <h1>argan oil</h1>
      <div className="earning-info">
        <h4>you've made</h4>
        <h4 className="amount">{calculateProfit()}</h4>
      </div>
      <div>
        <h5>What would you like to do?</h5>
      </div>
      <br/>
      <Button size="large" variant="contained" color="primary">
        <Link to="/register-oil" className="menu-item">Register Oil</Link>
      </Button>
      <Button size="large" variant="contained" color="primary" className="scanButton">
        <Link to="/claim-oil" className="menu-item">Scan</Link>
      </Button>
      <div>
        <h5>Your Certifications:</h5>
      </div>
      <CertificationList credentialStore={credentialStore} />
      <div>
        <h5>Oil:</h5>
      </div>
      {oilList(true)}
      <div>
        <h5>Oil (sold):</h5>
      </div>
      {oilList(false)}
    </div>
  );
}

export default HomeView;
