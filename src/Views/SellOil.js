import React, { useState } from 'react';
import { Button, TextField  } from '@material-ui/core';
import { useParams } from 'react-router-dom'
import ArganOilForSale from '../Components/Modals/ArganOilForSale';

function SellOilView({oilStore}) {

  const requireAnyData = false;
  const [disabledButton, disableButton] = useState(requireAnyData)
  const [activeModal, setModalActive] = useState(false)
  const [address, setAddress] = useState("")
  var { id } = useParams();

  let checkButtonDisable = () => {
    if (address !== '' && address.length >= 42) {
      disableButton(false);
    } else {
      disableButton(true);
    }
    if (!requireAnyData) {
      disableButton(false);
    }
  }

  let sellOil = async () => {
    disableButton(true);
    const result = await oilStore.sellOil(id, address)
    console.log(result);
    disableButton(false);
    if (result) {
      setModalActive(true);
    }
  }

  let fields = (
    <TextField label="Recievers Address" id="address" type="text" value={address} 
          onChange={event => {setAddress(event.target.value); checkButtonDisable();}}
          onBlur={event => {setAddress(event.target.value); checkButtonDisable();}}/>
  );
  if (!requireAnyData) fields = (<div></div>)

  const oilSaleForm = (
    <div className="div-table-container">
      <div className="div-table">
        <div className="div-table-column">
          {fields}
          <br/>
          <Button onClick={() => sellOil()} size="large" variant="contained" color="primary" disabled={disabledButton}>
            Sell Oil Passport
          </Button>
        </div>
      </div>
      <div className="div-table scrollable">
      </div>
    </div>
  )

  return (
    <div className="dashboard-view page">
      <h1>Sell Argan Oil</h1>
      <div className="horizontal-container">
        <ArganOilForSale id={id} oilStore={oilStore} active={activeModal} ></ArganOilForSale>
        {oilSaleForm}
      </div>
    </div>
  );
}

export default SellOilView;
