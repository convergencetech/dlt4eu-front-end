import React, { useState } from 'react';
import { Button, TextareaAutosize, TextField  } from '@material-ui/core';

function DashboardView({credentialStore, oilStore}) {

  const [credentialData, setCredentialData] = useState("")

  let storeCredential = () => {
    if (credentialData === "") return
    credentialStore.addCredential(credentialData)
    setCredentialData("")
  }

  let deleteCredential = (id) => {
    credentialStore.deleteCredential(id)
    setCredentialData("DELETED")
    setTimeout(function(){ setCredentialData(""); }, 500);
  }

  let credentialList = (<div></div>)

  if (credentialStore.credentials.length > 0) {
    credentialList = credentialStore.credentials.map((value, id) =>
      <li className="clickable-delete" key={id} onClick={() => deleteCredential(id)}>{value.issuedCredentials[0].data.transcript[0].name.split(":")[2]}</li>
    );
  }

  const credentialsTable = (
    <div className="div-table-container">
      Credentials: {credentialStore.credentials.length}
      <div className="div-table">
        <div className="div-table-column">
          <TextareaAutosize value={credentialData} onChange={event => setCredentialData(event.target.value)} aria-label="minimum height" rowsMin={3} rowsMax={10} placeholder="Paste Here" />
        </div>
        <div className="div-table-column">
          <Button onClick={() => storeCredential()} size="medium" variant="contained" color="primary">
            Add Credential
          </Button>
        </div>
      </div>
      <div className="div-table scrollable">
      <ul className="unmarked">{credentialList}</ul>
      </div>
    </div>
  )

  return (
    <div className="dashboard-view page">
      <h1>DASHBOARD</h1>
      <div className="horizontal-container">
        {credentialsTable}
      </div>
    </div>
  );
}

export default DashboardView;
