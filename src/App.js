import React, { useState, useEffect } from 'react';
import './App.scss';
import {BrowserRouter as Router} from "react-router-dom"
import Web3 from 'web3';

import NavigationSwitch from './Components/Navigation/NavigationSwitch'
import Header from './Components/PageParts/Header';
import Footer from './Components/PageParts/Footer';
import Login from './Components/Login';
import CredentialStore from './Components/Credentials/CredentialStore';
import OilStore from './Components/Oil/OilStore';

function App() {

  const [web3, setWeb3] = useState(false)
  const [authenticated, setAuthenticated] = useState(false)
  const [user, setUser] = useState(null)
  const [credentialStore, setCredentialstore] = useState(null)
  const [oilStore, setOilStore] = useState(null)
  const [arganOilContract, setArganOilContract] = useState(null)

  useEffect(() => {
    const web3 = new Web3(process.env.REACT_APP_WEB3_PROVIDER);
    setWeb3(web3)

    const storedUser = localStorage.getItem("user")
    if (storedUser != null) {
      let userData = JSON.parse(storedUser)
      setUser(userData)
    }

    const arganOilAbi = require('./Contracts/ArganOilAbi.json');
    const arganOilContract = new web3.eth.Contract(
      arganOilAbi,
      process.env.REACT_APP_ARGAN_OIL_CONTRACT_ADDRESS
    );
    setArganOilContract(arganOilContract);
    setCredentialstore(new CredentialStore());
    if (storedUser != null) {
      if (oilStore == null) setOilStore(new OilStore(JSON.parse(storedUser), web3, arganOilContract));
    }
  }, [oilStore])

  const registerUser = (user) => {
    setUser(user)
    if (user != null) {
      setOilStore(new OilStore(user, web3, arganOilContract));
    }
  }


  let page = (
    <Router>
      {/* <UserDetails user={user}/> */}
      <Login user={user} registerUser={registerUser} setAuthenticated={setAuthenticated} autoLogIn={true}/>
    </Router>
  );

  if (arganOilContract == null) {
    page = (
      <div>Loading...</div>
    );
  }

  if (user && authenticated) {
    page = (
      <Router>
      {/* <Header setAuthenticated={setAuthenticated}/> */}
      <NavigationSwitch credentialStore={credentialStore} oilStore={oilStore} web3={web3} user={user}/>
      {/* <Footer /> */}
      </Router>
    )
  }

  return (
    <div className="main">
      {page}
    </div>
  )
}

export default App;