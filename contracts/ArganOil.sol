pragma solidity >=0.4.22 <0.8.0;
pragma experimental ABIEncoderV2;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/cryptography/ECDSA.sol";

contract ArganOil is ERC721 {
    using ECDSA for bytes32;

    struct Oil {
        address minter;
        address currentOwner;
        uint volume;
        string oilType;
        uint price;
        bytes32 credentialsProof;
        bytes32 saleVerify;
    }

    mapping(uint => Oil) arganOilInfo;
    mapping(address => bytes32[]) nonceUsed;
 
    constructor() ERC721("ArganOil", "ARGO") public {
    }

    function mint(uint _volume, string memory _oilType, uint _price, bytes32 _credentialsProof) public {
        uint id = totalSupply()+1;
        _safeMint(msg.sender, id);
        Oil memory oil = Oil(msg.sender, msg.sender, _volume, _oilType, _price, _credentialsProof, 0);
        arganOilInfo[id] = oil;
    }

    /* 
    * Basic ERC721 functionality
    */

    function getVolume(uint256 _id) public view returns (uint) {
        require(_exists(_id), "Oil does not exist");
        return arganOilInfo[_id].volume;
    }

    function getOilType(uint256 _id) public view returns (string memory) {
        require(_exists(_id), "Oil does not exist");
        return arganOilInfo[_id].oilType;
    }

    function getPrice(uint256 _id) public view returns (uint) {
        require(_exists(_id), "Oil does not exist");
        return arganOilInfo[_id].price;
    }

    function getMinter(uint256 _id) public view returns (address) {
        require(_exists(_id), "Oil does not exist");
        return arganOilInfo[_id].minter;
    }

    function getCurrentOwner(uint256 _id) public view returns (address) {
        require(_exists(_id), "Oil does not exist");
        return arganOilInfo[_id].currentOwner;
    }

    function getOilInfo(uint256 _id) public view returns (Oil memory) {
        require(_exists(_id), "Oil does not exist");
        return arganOilInfo[_id];
    }

    function isOilForSale(uint256 _id) public view returns (bool) {
        require(_exists(_id), "Oil does not exist");
        return !validateNotForSale(_id);
    }

    /* 
    * Custom delegated functionality
    */
    function delegatedMint(address _sender, uint _volume, string memory _oilType, uint _price, bytes32 _credentialsProof, bytes32 _nonce, bytes memory _signedMessage) public {
        require(validateNonce(_sender, _nonce), "Nonce already used");
        require(validateMintData(_sender, _volume, _oilType, _price, _credentialsProof, _nonce, _signedMessage), "Owner did not sign the message");
        uint256 id = totalSupply()+1;
        _safeMint(_sender, id);
        Oil memory oil = Oil(_sender, _sender, _volume, _oilType, _price, _credentialsProof, 0);
        arganOilInfo[id] = oil;
        nonceUsed[_sender].push(_nonce);
    }

    function delegatedTransferFrom(address _from, address _to, uint256 _tokenId, bytes memory _signedMessage) public {
        require(_isApprovedOrOwner(_from, _tokenId), "From address is not the owner of the Oil");
        require(validateTransferData(_from, _to, _tokenId, _signedMessage), "Owner did not sign the message");
        require(validateNotForSale(_tokenId), "Oil is listed for sale and needs to be claimed first");
        _transfer(_from, _to, _tokenId);
        arganOilInfo[_tokenId].currentOwner = _to;
    }

    function delegatedSale(address _from, uint256 _tokenId, bytes32 _verify, bytes memory _signedMessage) public {
        require(_isApprovedOrOwner(_from, _tokenId), "From address is not the owner of the Oil");
        require(validateSaleData(_from, _tokenId, _verify, _signedMessage), "Owner did not sign the message");
        require(validateNotForSale(_tokenId), "Oil is already listed for sale and needs to be claimed first");
        arganOilInfo[_tokenId].saleVerify = _verify;
    }

    function delegatedClaim(address _from, uint256 _tokenId, bytes32 _nonce, bytes memory _signedMessage) public {
        require(!validateNotForSale(_tokenId), "Oil is not listed for sale");
        Oil memory oil = arganOilInfo[_tokenId];
        require(validateClaimData(_from, _tokenId, _nonce, _signedMessage), "Owner did not sign the message");
        require(validateClaim(_tokenId, oil.volume, oil.oilType, oil.price, _nonce, oil.saleVerify), "Verify did not match");
        oil.saleVerify = 0;
        address currentOwner = oil.currentOwner;
        _transfer(currentOwner, _from, _tokenId);
        oil.currentOwner = _from;
        arganOilInfo[_tokenId] = oil;
    }


    /* 
    * Delegated minting validation
    */
    function validateMintData(address _sender, uint _volume, string memory _oilType, uint _price, bytes32 _credentialsProof, bytes32 _nonce, bytes memory _signedMessage) internal pure returns (bool) {
        bytes32 messageHash = keccak256(abi.encodePacked(_volume, _oilType, _price, _nonce, _credentialsProof));
        string memory result = string(abi.encodePacked("\x19Ethereum Signed Message:\n32", messageHash));
        bytes32 check = keccak256(abi.encodePacked(result));
        address recoveredAddress = check.recover(_signedMessage);
        bool valid = false;
        if (_sender == recoveredAddress) {
            valid = true;
        }
        return valid;
    }

    /* 
    * Delegated transfer validation
    */
    function validateTransferData(address _from, address _to, uint _tokenId, bytes memory _signedMessage) internal pure returns (bool) {
        bytes32 messageHash = keccak256(abi.encodePacked(_to, _tokenId));
        string memory result = string(abi.encodePacked("\x19Ethereum Signed Message:\n32", messageHash));
        bytes32 check = keccak256(abi.encodePacked(result));
        address recoveredAddress = check.recover(_signedMessage);
        bool valid = false;
        if (_from == recoveredAddress) {
            valid = true;
        }
        return valid;
    }

    /* 
    * Delegated sale validation
    */
    function validateSaleData(address _from, uint256 _tokenId, bytes32 _verify, bytes memory _signedMessage) internal pure returns (bool) {
        bytes32 messageHash = keccak256(abi.encodePacked(_tokenId, _verify));
        string memory result = string(abi.encodePacked("\x19Ethereum Signed Message:\n32", messageHash));
        bytes32 check = keccak256(abi.encodePacked(result));
        address recoveredAddress = check.recover(_signedMessage);
        bool valid = false;
        if (_from == recoveredAddress) {
            valid = true;
        }
        return valid;
    }

    /* 
    * Validate it is not for sale
    */
    function validateNotForSale(uint256 tokenId) internal view returns (bool) {
        bool valid = false;
        if (arganOilInfo[tokenId].saleVerify == 0) valid = true;
        return valid;
    }

    /* 
    * Delegated claim validation
    */
    function validateClaimData(address _from, uint256 _tokenId, bytes32 _nonce, bytes memory _signedMessage) internal pure returns (bool) {
        bytes32 messageHash = keccak256(abi.encodePacked(_tokenId, _nonce));
        string memory result = string(abi.encodePacked("\x19Ethereum Signed Message:\n32", messageHash));
        bytes32 check = keccak256(abi.encodePacked(result));
        address recoveredAddress = check.recover(_signedMessage);
        bool valid = false;
        if (_from == recoveredAddress) {
            valid = true;
        }
        return valid;
    }

    function validateClaim(uint256 _tokenId, uint _volume, string memory _oilType, uint price, bytes32 _nonce, bytes32 _saleVerify) internal pure returns (bool) {
        bytes32 messageHash = keccak256(abi.encodePacked(_tokenId, _volume, _oilType, price, _nonce));
        bool valid = false;
        if (messageHash == _saleVerify) {
            valid = true;
        }
        return valid;
    }    

    function validateVerify(address _from, uint256 _tokenId, uint _volume, string memory _oilType, uint price, bytes32 _nonce, bytes32 _saleVerify) internal pure returns (bool) {
        bytes32 messageHash = keccak256(abi.encodePacked(_tokenId, _from, _volume, _oilType, price, _nonce));
        bool valid = false;
        if (messageHash == _saleVerify) {
            valid = true;
        }
        return valid;
    }    

    /*
    * NONCE
    */

    function validateNonce(address _sender, bytes32 nonce) internal view returns (bool) {
        bytes32[] memory noncesUsed = nonceUsed[_sender];
        for (uint i=0; i<noncesUsed.length; i++) {
            if (nonce == noncesUsed[i]) return false;
        }
        return true;
    }
}